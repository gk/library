/* Copyright 2019--2020 The Tor Project
 * Copyright 2021 SR2 Communications Limited
 * See LICENSE for licensing information */

package org.torproject.descriptor.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.torproject.descriptor.BridgestrapStats;
import org.torproject.descriptor.BridgestrapTestResult;
import org.torproject.descriptor.DescriptorParseException;

import org.apache.commons.compress.utils.IOUtils;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

public class BridgestrapStatsImplTest {

  @Test
  public void testParse() throws IOException, DescriptorParseException {
    URL resource = getClass().getClassLoader().getResource(
        "bridgestrap/bridgestrap-collector");
    assertNotNull(resource);
    InputStream dataInputStream = resource.openStream();
    assertNotNull(dataInputStream);
    byte[] rawDescriptorBytes = IOUtils.toByteArray(dataInputStream);
    BridgestrapStats bridgestrapStats = new BridgestrapStatsImpl(
        rawDescriptorBytes, new int[]{0, rawDescriptorBytes.length},
        null);
    assertEquals(
        LocalDateTime.of(
          LocalDate.of(2021, 7, 20),
          LocalTime.of(19, 11, 16)),
        bridgestrapStats.bridgestrapStatsEnd());
    assertEquals(5051,
        bridgestrapStats.bridgestrapCachedRequests());
    Optional<List<BridgestrapTestResult>> resultList =
        bridgestrapStats.bridgestrapTests();
    assertTrue(resultList.isPresent());
    assertEquals(3281, resultList.get().size());
    BridgestrapTestResult first = resultList.get().get(0);
    assertFalse(first.isReachable());
    assertTrue(first.hashedFingerprint().isPresent());
    assertEquals(
        "00795E6F05824B2DE96411735BF5D7A163CE63C2",
        first.hashedFingerprint().get());
    BridgestrapTestResult second = resultList.get().get(1);
    assertFalse(second.isReachable());
    assertTrue(second.hashedFingerprint().isPresent());
    assertEquals(
        "0089AD6FED844EA585DCC5127B6BD757C34F22C3",
        second.hashedFingerprint().get());
    BridgestrapTestResult last = resultList.get().get(
        resultList.get().size() - 1);
    assertTrue(last.isReachable());
    assertTrue(last.hashedFingerprint().isPresent());
    assertEquals(
        "FFD817FAAA36F185F43474F74C446EA8D95EC38F",
        last.hashedFingerprint().get());
  }
}
